/**
 * Created by sduvaud on 26/05/16.
 */

var info = require('./beacon-info.js');

info.getDatasetsFromArrayMap(function (callback){
    infoObject = callback;
});

var infoObject = {message : "loading datasets from arraymap api, please refresh the page in a few seconds..."};

function checkDatasetIdentifier(params) {

    if (!params.info && !params.id) {
        return {
            hasError: true,
            msg: "No identifier specified"
        };
    }
    return {
        hasError: false
    };
}

function getDatasets(params) {
    if (params.info) {
        return infoObject;
    }
    if (params.id == 'all') {
        return infoObject.info.info.datasets;
    }
    else {
        var found = {};
        var datasets = infoObject.info.info.datasets;
        var identifiers = params.id.split(','); // comma separated list of datasets
        var output = [];
        identifiers.forEach(function (id) {
            found = {};
            datasets.forEach(function (dataset) {
                if (dataset.id == id) {
                    found = dataset;
                }
            });
            if (JSON.stringify(found)=='{}') {
                var json =  {
                    "id": id,
                    "description": "Not found",
                    "reference": "reference genome",
                    "size": {
                        "variants": "-1",
                        "samples": 0
                    }
                };
                output.push(json);
            }
            else {
                output.push(found);
            }
        });
        return output;

    }
}
module.exports.checkDatasetIdentifier = checkDatasetIdentifier;
module.exports.getDatasets = getDatasets;