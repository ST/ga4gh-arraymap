// More functional test than Unit tests
// Error with the HTTP request to the API
// (empty object returned due to async)

var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../app');
var should = chai.should();

chai.use(chaiHttp);

describe('Beacon arrayMap', function() {
    it('should display the info page', function(done) {
        chai.request(server)
            .get('/info')
            .end(function(err, res){
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.should.not.be.empty;
                res.should.have.property('info');
                done();
            });
    });
    it('should display the sample page, 10 first samples' , function(done) {
        chai.request(server)
            .get('/samples?limit=10')
            .end(function(err, res){
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('array');
                res.body.should.have.lengthOf(10);
                res.body[0].should.have.property('_id');
                res.body[0].should.have.property('DIAGNOSISTEXT');
                done();
            });
    });
    it('should display the query page' , function(done) {
        chai.request(server)
            .get('/v0.4/query')
            .end(function(err, res){
                res.should.have.status(200);
                res.body.should.equal('reference name is not present');
                done();
            });
    });
});