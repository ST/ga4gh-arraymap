var cities = [];

db.samples.find().forEach(function (sample) {
    cities.push(sample.CITY);
});

var uniqueCities = _.uniq(cities);
prettyPrint(uniqueCities);

var countyForCity = _.countBy(cities);
prettyPrint(countyForCity);
