var arrayMap = angular.module('beaconArrayApp', ['ngRoute', 'ng-showdown']);
arrayMap.config(['$routeProvider', '$locationProvider',
    function($routeProvider, $locationProvider) {

        $locationProvider.html5Mode(true);

        $routeProvider.
        when('/', {
            templateUrl: 'partials/beacon-home.html'
        }).
        when('/documentation', {
            templateUrl: 'partials/beacon-doc.html'
        });
    }]);

arrayMap.factory('BeaconService', ['$http', '$q', function ($http, $q) {
    var service = {
        getDatasets: getDatasets
    };
    return service;

    function getDatasets() {

        var defer = $q.defer();
        // var url = "http://arraymap.org/qsubsets/?db=arraymap&collection=subsets&subsettype=ICDMORPHOLOGYCODE&querytext=^8|9";
        var url = "/dataset?id=all";

        var req = {
            method: 'GET',
            url: url
        };

        //return $http.get(url)
        return $http(req)
            .then(function(response){
                return response.data;
            },
            function(){
                console.log("response not ok");
                return defer.reject("Error, arrayMap data unavailable");
            }
        );
        return defer.promise;
    }
}]);

arrayMap.controller('BeaconController', ['$scope', '$location', 'BeaconService', function ($scope, $location, BeaconService) {

    $scope.references = ['GRCh36','GRCh37','GRCh38'];

    $scope.config = {
        "referenceName": "9",
        "start": 42049214,
        "assemblyId": "GRCh36",
        "datasetIds": "9440/3",
        "alternateBases": "DEL",
        "length": 26740,
        "confidenceIntervalStart": 500,
        "confidenceIntervalEnd": 500,
        "matchType": "any"
    };

    $scope.datasetConfig = {
        "id": "all"
    };

    $scope.getNewApiUrl = function () {

        var conf = $scope.config;
        var end = parseInt(conf.start) + parseInt(conf.length) - 1;

        return $location.absUrl() + "query?" +
            "referenceName=" + conf.referenceName +
            "&start=" + conf.start +
            "&assemblyId=" + conf.assemblyId +
            "&datasetIds=" + conf.datasetIds +
            ((conf.alternateBases === "") ? "all" : "&alternateBases=" + conf.alternateBases) +
            "&alternateBasesInfo='" +
                "END=" + end +
                ";SVLEN=" + conf.length +
                ";CIPOS=-" + conf.confidenceIntervalStart + "," + conf.confidenceIntervalStart +
                ";CIEND=-" + conf.confidenceIntervalEnd + "," + conf.confidenceIntervalEnd + "'" +
            "&matchType=" + conf.matchType;
    };

    $scope.getInfoUrl = function () {
        return $location.absUrl() + "info"
    };

    $scope.getDatasetUrl = function () {

        var conf = $scope.datasetConfig;
        return $location.absUrl() + "dataset?" +
                "id=" + conf.id;
    };

    $scope.getApiUrl = function () {

        var conf = $scope.config;

        return $location.absUrl() + "v0.4/query?chromosome=" +
            conf.chromosome +
            "&position=" + conf.position +
            "&reference=" + conf.reference +
            "&dataset=" + conf.dataset +
            ((conf.variantClass === "") ? "" : "&variantClass=" + conf.variantClass);
    };

    BeaconService.getDatasets()
        .then(function (datasets) {
            $scope.datasets = datasets;
        },
        function(datasets){
            console.log("Error getting datasets");
        }
    );
}]);


arrayMap.controller('DocumentationCtrl', ['$scope', '$http', function ($scope, $http) {
    $http.get('/pages/doc.md').success(function (data){
        $scope.mkText = data;
    });
}]);
