// This is the list of all the datasets. Required in many places to  check the
// dataset identifiers's validity for instance.
// Calling via localhost, since Beacon here always addresses the same host; safe for 
// testing purposes (e.g. local usually via arraymap.test).

var request = require("request");
var url = "http://127.0.0.1/qsubsets/?db=arraymap&collection=subsets&subsettype=ICDMORPHOLOGYCODE&querytext=^8|9";

module.exports = {
	getDatasetsFromArrayMap: function(callback) {
		var object;
		request(url, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				// Incorrect json returned.
				// Remove flanking parenthesis
				body = body.replace(/^\(/, "");
				body = body.replace(/\}\);/, "}");
				object = getDatasets(JSON.parse(body));
				callback(object);
			}
			else {
				console.log("STATUS CODE: " + response.statusCode);
				console.log("ERROR: " + error);
				throw error;
			}
		});
	}
};

// I cannot remember why such a complicated structure...
// Does it come from the Beacon API specification for 0.4?
// Will have to figure out...
function getDatasets(json) {

	var object = {};
	var info = {};

	var infoinfo = {};
	infoinfo.id = "arraymap-beacon";
	infoinfo.name = "arrayMap Beacon";
	infoinfo.organization = "SIB Swiss Institute of Bioinformatics";
	infoinfo.description = "First Prototype of a Beacon v0.4 implementation for arrayMap";
	infoinfo.api = "v0.4";
	infoinfo.homepage =  "http://beacon.arraymap.org/";
	infoinfo.email = "SIB-Technology@isb-sib.ch";

	var datasets = [];
	var array = json.subsetdata;

	// TODO: sort array by SUBSETCODE (ATM done in the called CGI)

	for (var i = 0; i < array.length; i++) {

		var element = array[i];
		var dataset = {};

		// "UID":"80003","SUBSETCODE":"8000/3","SAMPLENO":7,"SUBSETTEXT":"8000/3: Neoplasm, malignant"

		dataset.id = element.SUBSETCODE;
		dataset.description = element.SUBSETTEXT+" ("+element.SAMPLENO+")";
		dataset.reference = "reference genome";

		var size = {};
		size.variants = "-1";
		size.samples = element.SAMPLENO;

		if (size.samples > 0) {

			dataset.size = size;
			datasets.push(dataset);
			
		}

	}
	infoinfo.datasets = datasets;
	info.info = infoinfo;
	object.info = info;

	return object;
}
