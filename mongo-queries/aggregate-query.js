/**
 * Created by sduvaud on 25/05/16.
 */

var query = db.samples.aggregate(
    [
        {
            "$match": {
                "$and":
                    [
                        {
                            "SEGMENTS_HG38":{
                                "$elemMatch":{"CHRO":"11","SEGTYPE":-1,"SEGSTOP":{"$gte":34439881},"SEGSTART":{"$lte":34439881}}
                            }
                        },
                        {
                            "$or":[
                                {
                                    "ICDMORPHOLOGYCODE":"8041/3"
                                },
                                {
                                    "ICDMORPHOLOGYCODE":"8070/3"
                                }
                            ]
                        }
                    ]
            }
        },
        {
            "$group": {
                _id: "$ICDMORPHOLOGYCODE",
                // Number of samples with at least one segment
                // Datasets with no sample are not shown
                // This is of an issue when the user asks for
                // all datasets.
                total : { $sum : 1 }
            }
        }
    ]);

query.forEach(function (match) {

    //prettyPrint(match);
})


var query2 = db.samples.aggregate(
    [
        {
            $match:
                { $or: [
                        {ICDMORPHOLOGYCODE: '8070/3' },
                        {ICDMORPHOLOGYCODE: '8041/3' }
                    ]
                }
        },
        {
            $group: {
                _id: "$ICDMORPHOLOGYCODE",
                total: { $sum: 1 }
            }
        }
    ]
)
query2.forEach(function (match) {
    prettyPrint(match);
})

    /*
     db.orders.aggregate( [
     {
     $group: {
     _id: "$ICDMORPHOLOGYCODE",
     total: { $sum: 1 }
     }
     }
     ] )


     [{"$match":{"$and":[{"SEGMENTS_HG38":{"$elemMatch":{"CHRO":"11","SEGTYPE":-1,"SEGSTOP":{"$gte":34439881},"SEGSTART":{"$lte":34439881}}}},{"$or":[{"ICDMORPHOLOGYCODE":"8070/3"},{"ICDMORPHOLOGYCODE":"8041/3"}]}]}},{"$group":{"_id":"$ICDMORPHOLOGYCODE","observed":{"$sum":1}}}]

     [{"$match":{"$and":[{"SEGMENTS_HG38":{"$elemMatch":{"CHRO":"11","SEGTYPE":-1,"SEGSTOP":{"$gte":34439881},"SEGSTART":{"$lte":34439881}}}},{"$or":[{"ICDMORPHOLOGYCODE":"8070/3"}]}]}},{"$group":{"_id":"$ICDMORPHOLOGYCODE","observed":{"$sum":1}}}]



     Building MongoDB query params: {"$and":[{"SEGMENTS_HG38":{"$elemMatch":{"CHRO":"11","SEGTYPE":-1,"SEGSTOP":{"$gte":34439881},"SEGSTART":{"$lte":34439881}}}},{"$or":[{"ICDMORPHOLOGYCODE":"8070/3"}]}]}





     var matches = [];

     var groupByQuery = db.samples.aggregate(
     [
     { $group : { _id : "$ICDMORPHOLOGYCODE", description: { $first: "$ICDMORPHOLOGY"}, uid: { $push: "$UID" } } }
     ]
     );

     groupByQuery.forEach(function (match) {
     var samples = match.uid;
     var description = match.description;
     var genome = 'reference genome';

     matches.push('\n{"id": "' + match._id + '", "description": "' + description+ '", "reference": "' + genome + '", "size": { "variants": "-1", "samples": "' + samples.length + '"}}');
     });

     print ('{ "info": {\n "id": "arraymap-beacon",\n "name": "Beacon arrayMap", \n "organization": "SIB Swiss Institute of Bioinformatics",\n "description": "First Prototype of a Beacon v0.2 implementation for arrayMap.",\n "datasets": [');
     print(matches);
     print ('],\n "api": "v0.2",\n "homepage": "http://beacon-arraymap.vital-it.ch/",\n "email": "SIB-Technology@isb-sib.ch"\n }\n}');



    var response = beacon.checkResultAndGetResponse(req.query, docs, count);
     response.NOT_BEACON_totalInDataSet = count;
     res.json(response);






        db.collection.aggregate(
            [
                {"$match":{
                    "$and":[
                        {
                            "$or":[
                                {'SYMPTOM_1': '477'},
                                {'SYMPTOM_2': '4770'}
                            ]
                        },
                        {
                            "$or":[
                                {'SYMPTOM_1': '147'},
                                {'SYMPTOM_2': '1470'}
                            ]
                        },
                    ]
                }
                },
                { "$sort": { "APPL_DATE": 1}},
                { "$group":
                {
                    "_id": "$_id",
                    "dis": {  "$push": { SYMPTOM_1: "$SYMPTOM_1", SYMPTOM_2: "$SYMPTOM_2" }}
                }
                }
            ]
        )

    res.json(docs);
    */



