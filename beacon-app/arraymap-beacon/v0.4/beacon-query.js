var info = require('./beacon-info.js');


info.getDatasetsFromArrayMap(function (callback){
    var datasets = callback.info.info.datasets;
    for (var i = 0; i < datasets.length; i++) {
        allDatasets.push({id: datasets[i].id, nbSamples: datasets[i].size.samples});
    }
});

var allDatasets = [];

var referenceMap = {
    'GRCh36': 'SEGMENTS_HG18',
    'GRCh37': 'SEGMENTS_HG19',
    'GRCh38': 'SEGMENTS_HG38'
};

//correspondence to arraymap class
var alternateBasesMap = {
    "DEL": -1,
    "DUP": 1
};

/*
  By default doe Maximilian takes Reference 37. https://github.com/maximilianh/ucscBeacon/blob/master/help.txt
  TODO Should it be the same as Maximilian? Is it specified through the Beacon documentation?
*/

var defaultReference = 'GRCh37';

function checkPreconditions(params) {

    if (!params.referenceName) {
        return {
            hasError: true,
            msg: "reference name is not present"
        };
    }

    var referenceNameInvalid = {
        hasError: true,
        msg: params.referenceName + " reference name not valid"
    };

    if (!isNaN(params.referenceName)) {
        if (params.referenceName < 1 || params.referenceName > 23) {
            return referenceNameInvalid;
        }
    }
    else {
        if (params.referenceName.match(/^(X|Y)$/i) == null) {
            return referenceNameInvalid;
        }
    }

    if (!params.start) {
        return {
            hasError: true,
            msg: "position not present"
        };
    }

    if (isNaN(params.start)) {
        return {
            hasError: true,
            msg: "position not a number"
        };
    }

    if (!params.datasetIds) {
        return {
            hasError: true,
            msg: "No dataset provided"
        };
    }

    if (params.datasetIds != 'all') {
        var allDatasetIdentifiers = [];
        allDatasets.forEach(function(dataset){
            allDatasetIdentifiers.push(dataset.id);
        });
        var error = checkDatasetIdentifiers(params.datasetIds, allDatasetIdentifiers);
        if (error.length > 0) {
            return {
                hasError: true,
                msg: "Incorrect dataset(s): " + error.toString()
            };
        }
    }

    if (!params.alternateBases) {
        return {
            hasError: true,
            msg: "Alternate bases not defined, arrayMap supports DUP or DEL followed by optional length"
        };
    }

    // Beacon 0.4
    // alternate bases: DEL[0-9]* AND DUP[0-9]*
    var alternate = params.alternateBases.replace(/\d+/, "");
    if (!alternateBasesMap[alternate]) {
        return {
            hasError: true,
            msg: "Alternate bases not supported, arrayMap supports DUP or DEL followed by optional length"
        };
    }

    //TODO add additional checks

    return {
        hasError: false
    };
}

/*
* Latest version of API v-0.4
*
* BeaconAlleleRequest:
* referenceName
* start
* alternateBases
* assemblyId
* datasetIds
*
* BeaconAlleleResponse
* beaconId
* beaconAlleleRequest
* beacondatasetAlleleResponse[]
* beaconError
*
* BeaconDatasetAlleleResponse
* datasetId
* exists
* beaconError
* note
*
* */
function buildMongoQuery(params) {

/*
* ALTBASEINFO: 'END=42134902;SVLEN=85689;CIPOS=-500,500;CIEND=-500,500'
* MATCHTYPE: any
*/
    console.log("ALTBASEINFO: " + params.alternateBasesInfo);
    console.log("MATCHTYPE: " + params.matchType);

    var position = parseInt(params.start, 10);
    var length = params.alternateBases.replace(/(DUP|DEL)/, "") ? parseInt(params.alternateBases.replace(/(DUP|DEL)/, ""), 10) : 0;

    var andConditions = [];
    var orConditions  = [];

    //add constraint on datasets if required
    if (params.datasetIds) {
        if (params.datasetIds != 'all') {
            var identifiers = params.datasetIds.split(','); // comma separated list of datasets
            identifiers.forEach(function (id) {
                orConditions.push({ICDMORPHOLOGYCODE: id});
            });
        }
    }

    // Beacon 0.4
    // alternate bases: DEL[0-9]* AND DUP[0-9]*
    var segType = alternateBasesMap[params.alternateBases.replace(/\d+/, "")];
    var convertedReference = referenceMap[params.assemblyId || defaultReference];

    var condition = {};
    condition[convertedReference] = {
        '$elemMatch': {
            'CHRO': params.referenceName,
            'SEGTYPE': segType
        }
    };

    //elem match element
    var condElemMatch = condition[convertedReference]['$elemMatch'];
    if (length > 0) {
        condElemMatch.SEGSIZE  = length;
        condElemMatch.SEGSTART = position;
    }
    else {
        condElemMatch.SEGSTOP  = {$gte: position};
        condElemMatch.SEGSTART = {$lte: position};
    }

    andConditions.push(condition);
    if (orConditions.length > 0) {
        andConditions.push({$or: orConditions});
    }

    return [
        {
            "$match": {
                "$and": andConditions
            }
        },
        {
            "$group": {
                _id: "$ICDMORPHOLOGYCODE",
                // Number of samples with at least one segment
                // Datasets with no sample are not shown
                // This is of an issue when the user asks for
                // all datasets.
                observed : { $sum : 1 }
            }
        }
    ];
}

function checkResultAndGetResponse(params, datasets) {

    var responses = [];

    // The query returns only datasets for which there is at least one sample with at least one matching SEGMENT...
    // What should we do with "unmatched" datasets?
    // Should we show them, as well with an exists: false?
    // what about cases where all the datasets are requested?

    if (datasets && datasets.length > 0)
    {
        // var matchedDatasets = datasets.map(function (s) { return checkResult(params, s)}); // checks to be added!
        var matchedDatasets = checkResult(params, datasets);
        var response;

        if (typeof matchedDatasets == undefined && matchedDatasets == null) {
            response = {
                "exists": null,
                "datasetId": null,
                "sampleCount": 0,
                "frequency": 0,
                "error": {
                    errorCode: 500,
                    message: "Internal error, DB returned a value but post check is not valid."
                },
                "note": null
            };
            responses.push(response);
        }else {
            matchedDatasets.forEach(function (dataset) {
                if (dataset.observed == 0) {
                    response = {
                        "exists": false,
                        "error": null,
                        "note": null,
                        "datasetId": dataset._id,
                        "sampleCount": 0,
                        "frequency": 0
                    };
                }else {
                    var totalSample = getTotalNbSample(dataset._id, allDatasets);
                    var frequency = totalSample > 1 ? (dataset.observed / totalSample).toPrecision(3) : 0;
                    response = {
                        "exists": true,
                        "error": null,
                        "note": null,
                        "datasetId": dataset._id,
                        "sampleCount": dataset.observed,
                        "frequency": frequency
                    };
                }
                responses.push(response);
            })
        }
    }else {

        response = {
            "exists": false,
            "datasetId": null,
            "sampleCount": 0,
            "frequency": 0,
            "error": null,
        };

        // Beacon 0.4
        // alternate bases: DEL[0-9]* AND DUP[0-9]*
        if (!alternateBasesMap[params.alternateBases.replace(/\d+/,"")]) {
            response.note = "Type of variant not supported by arrayMap.";
        }
        else {
            response.note = "No result from arrayMap.";
        }

        responses.push(response);
    }

    var queryResource = {
        "referenceName": params.referenceName,
        "start": params.start,
        "assemblyId": params.assemblyId,
        "datasetIds": params.datasetIds,
        // Beacon 0.4
        // alternate bases: DEL[0-9]* AND DUP[0-9]*
        "alternateBases": params.alternateBases
    };

    // BeaconAlleleResponse
    return {
        "beaconId": 'arraymap-beacon',
        "datasetAlleleResponses": responses,
        "alleleRequest": queryResource,
        "error": null
    };
}

function getTotalNbSample(id, allDatasets) {
    var nbSamples = 1; // and not 0: Division
    allDatasets.forEach(function(dataset){
        if (dataset.id == id) {
            nbSamples = dataset.nbSamples;
        }
    });
    return nbSamples;
}

// checks to be added!
// Pending for now!
function checkResult(params, datasets) {

    var allDatasets = datasets;

    var matchedIdentifiers = [];
    datasets.forEach(function(dataset){
        matchedIdentifiers.push(dataset._id);
    });

    var submittedIdentifiers = [];
    allDatasets.forEach(function(datasetId){
        submittedIdentifiers.push(datasetId.id);
    });
    if (params.datasetIds != 'all') {
        submittedIdentifiers = params.datasetIds.split(',');
    }

    var missedIdentifiers = checkDatasetIdentifiers(submittedIdentifiers.toString(), matchedIdentifiers);
    if (missedIdentifiers != null) {
        missedIdentifiers.forEach(function (missed) {
            var missedObj = {
                "_id": missed,
                "observed": 0
            };
            allDatasets.push(missedObj);
        });
    }
    return allDatasets;
}

function checkDatasetIdentifiers(userDatasetIdentifiers, allDatasetIdentifiers) {
    var identifiers = userDatasetIdentifiers.split(','); // comma separated list of datasets
    var found;
    var error = [];
    identifiers.forEach(function (id) {
        found = false;
        allDatasetIdentifiers.forEach(function (all) {
            if (all == id) {
                found = true;
            }
        });
        if (!found) {
            error.push(id);
        }
    });

    return error;
}

module.exports.checkPreconditions = checkPreconditions;
module.exports.buildMongoQuery = buildMongoQuery;
module.exports.checkResultAndGetResponse = checkResultAndGetResponse;
