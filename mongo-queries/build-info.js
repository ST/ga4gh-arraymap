/*
 Created by sduvaud on 23/02/16.
 modified by mbaudis on 2016-08-04
*/

var matches = [];

var groupByQuery = db.samples.aggregate(
    [
        { $group : { _id : "$ICDMORPHOLOGYCODE", description: { $first: "$ICDMORPHOLOGY"}, uid: { $push: "$UID" } } }
    ]
);

groupByQuery.forEach(function (match) {
    var samples = match.uid;
    var description = match.description;
    var genome = 'reference genome';

    matches.push('\n{"id": "' + match._id + '", "description": "' + description+ '", "reference": "' + genome + '", "size": { "variants": "-1", "samples": "' + samples.length + '"}}');
});

print ('var object = {\n\tinfo: { "info": {\n\t\t"id": "arraymap-beacon",\n\t\t"name": "Beacon arrayMap",\n\t\t"organization": "SIB Swiss Institute of Bioinformatics",\n\t\t"description": "First Prototype of a Beacon v0.4 implementation for arrayMap",\n\t\t"api": "v0.4",\n\t\t"homepage": "http://beacon.arraymap.org/",\n\t\t"email": "SIB-Technology@isb-sib.ch",\n\t\t"datasets": [');
print(matches);
print ('\t\t]\n\t}\n\t}\n};');
print ('module.exports.info = object;\n');
