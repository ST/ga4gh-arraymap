var beacon = {};

beacon.info = require('./beacon-info.js').getDatasetsFromArrayMap;

beacon.checkResultAndGetResponse = require('./beacon-query.js').checkResultAndGetResponse;
beacon.checkPreconditions = require('./beacon-query.js').checkPreconditions;
beacon.buildMongoQuery = require('./beacon-query.js').buildMongoQuery;

beacon.checkDatasetIdentifier = require('./beacon-dataset.js').checkDatasetIdentifier;
beacon.getDatasets = require('./beacon-dataset.js').getDatasets;

module.exports = beacon;