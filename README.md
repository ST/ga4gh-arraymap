Beacon queries and interface for the MongoDB based _[arrayMap](http://arraymap.org/ "arrayMap, visualizing cancer genome array data")_ cancer genome repository.

The structure of the project is the following:


# mongo-queries:

Queries are contained inside the directory [mongo-queries](mongo-queries)

#### Download commands

The following command can be used to download a chunk of the _arrayMap_ dataset, e.g. to understand data structures.

```
wget "http://arraymap.org/api/?db=arraymap&qcveto=1&api_out=samples&icdm_m=8,9&api_doctype=jsondata&randno=200" -O 200samples.json
```

The next command would replace the current local _arrayMap_ instance with the downloaded file.

```
mongoimport --drop --db arraymap --collection samples < 200samples.json --batchSize 1
```

# beacon-app

The directory contains the Beacon application itself. 

The application is based on node and angular.

In order to run the application on your localhost, you need to have [npm](https://www.npmjs.com/ "Package manager for JavaScript") installed.

Then, run:

```
npm install
```

in order to get all the required Javascript packages (as listed in package.json), and then:

```
npm start
```

You should also have a __arrayMap__ MongoDB running on your __localhost__, on port __27017__ (see above).

This can be changed in the application container:

```
app.js
```

#### bin

Contains the www script which is used by 

```
npm start
```

to start the server.

#### arraymap-beacon

Contains the implementation of the Beacon API. There is one sub-directory per API version.

#### test

Contains the functional tests (based on the [Chai](http://chaijs.com/ "Chai assertion library for node" ) assertion library).

#### public

Contains the scripts, CSS, images, HTML for the UI part.

#### routes

Contains the routing script.

#### views

Contains jade files. Jade is used as a view engine to display nice error pages in case an error is thrown (instead of raw stacktraces).