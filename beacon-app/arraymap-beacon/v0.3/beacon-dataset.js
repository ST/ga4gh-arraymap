/**
 * Created by sduvaud on 26/05/16.
 */

var info = require('./beacon-info.js').info;

// store all the dataset identifiers
var allDatasetIds = [];
info['info']['info']['datasets'].forEach(function(dataset){
    allDatasetIds.push(dataset.id);
});

function checkDatasetIdentifier(params) {

    if (!params.id) {
        return {
            hasError: true,
            msg: "No identifier specified"
        };
    }
    return {
        hasError: false
    };
}

function buildMongoDatasetQuery(params)
{
    console.log("ID=" + params.id);

    var groupCondition = {
        $group: {
            _id: "$ICDMORPHOLOGYCODE",
            sampleCount: {$sum: 1},
            name: { $first: "$ICDMORPHOLOGY"}
        }
    };

    if (params.id == 'all') {
        return [
            groupCondition
        ];
    }
    else {

        var orConditions = [];
        var identifiers = params.id.split(','); // comma separated list of datasets
        identifiers.forEach(function (id) {
            orConditions.push({ICDMORPHOLOGYCODE: id});
        });

        return [
            {
                $match: {
                    $or: orConditions
                }
            },
            groupCondition
        ];
    }
}

function checkDatasetResultAndGetResponse(params, datasets) {

    var matchedIdentifiers = [];
    datasets.forEach(function(dataset){
        matchedIdentifiers.push(dataset._id);
    });

    var submittedIdentifiers = allDatasetIds;
    if (params.id != 'all') {
        submittedIdentifiers = params.id.split(',');
    }

    if (submittedIdentifiers.length != matchedIdentifiers.length) {
        var missingIdentifiers = [];
        submittedIdentifiers.forEach(function(submitted) {
                var found = false;
                matchedIdentifiers.forEach(function(matched) {
                        if (matched == submitted) {
                            found = true;
                        }
                    }
                );
                if (!found) {
                    missingIdentifiers.push(submitted);
                }
            }
        );
        missingIdentifiers.forEach(function (missing) {
                var json =  {
                    "_id": missing,
                    "sampleCount": 0,
                    "name": "Dataset not found"
                };
                datasets.push(json);
            }
        );
    }
    return datasets;
}

module.exports.checkDatasetIdentifier = checkDatasetIdentifier;
module.exports.buildMongoDatasetQuery = buildMongoDatasetQuery;
module.exports.checkDatasetResultAndGetResponse = checkDatasetResultAndGetResponse;