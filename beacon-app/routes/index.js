var express = require('express');
var router = express.Router();
var _ = require('underscore')
var version = 'v0.4';
var beacon = require('../arraymap-beacon/' + version + '/arraymap-beacon.js');

/* GET home page. */
router.get('/', function (req, res, next) {
    res.sendfile('index.html');
});

/* GET doc page. */
router.get('/documentation', function (req, res, next) {
    res.sendfile('public/index.html');
});

/* GET info page. */
router.get('/info', function (req, res, next) {
    res.redirect('/dataset?info=true');
    //res.json(beacon.info);
});

/* Shows samples */
router.get('/samples/', function (req, res) {
    var db = req.db;
    if (req.query.limit) {
        db.samples.find({}).limit(parseInt(req.query.limit)).skip(1, function (e, docs) {
            res.json(docs);
        });
    } else {
        db.samples.find({}, {}, function (e, docs) {
            res.json(docs);
        });
    }
});

/* Returns response for API v 0.2  */
router.get('/v0.2/query/', function (req, res) {
    var beacon = require('../arraymap-beacon/v0.2/arraymap-beacon.js');
    var preconditions = beacon.checkPreconditions(req.query);

    if (preconditions.hasError) {
        res.json(preconditions.msg); //Gets error messages
        return;
    }
    var mongoQuery = beacon.buildMongoQuery(req.query);
    console.log("Building MongoDB query params: " + JSON.stringify(mongoQuery));

    req.db.samples.count({'ICDMORPHOLOGYCODE': req.query.dataset}, function(err, count){
            req.db.samples.find(mongoQuery, {}, function (err, docs){
            var response = beacon.checkResultAndGetResponse(req.query, docs, count);
            response.NOT_BEACON_totalInDataSet = count;
            res.json(response);
        });
    }
    )

});

/* Returns response for API v 0.3  */
router.get('/v0.3/query/', function (req, res) {
    beacon = require('../arraymap-beacon/v0.3/arraymap-beacon.js');
    var preconditions = beacon.checkPreconditions(req.query);

    if (preconditions.hasError) {
        res.json(preconditions.msg); //Gets error messages
        return;
    }

    var mongoQuery = beacon.buildMongoQuery(req.query);
    console.log("Building MongoDB query params: " + JSON.stringify(mongoQuery));

    req.db.samples.aggregate(mongoQuery, function(err, docs) {
        var response = beacon.checkResultAndGetResponse(req.query, docs);
        res.json(response);
    });
});

router.get('/v0.4/dataset', function (req, res) {
    beacon = require('../arraymap-beacon/v0.4/arraymap-beacon.js');
    var preconditions = beacon.checkDatasetIdentifier(req.query);

    if (preconditions.hasError) {
        res.json(preconditions.msg);
        return;
    }

    var datasets = beacon.getDatasets(req.query);
    res.json(datasets);
});

/* Returns response for API v 0.4  */
router.get('/v0.4/query/', function (req, res) {
    beacon = require('../arraymap-beacon/v0.4/arraymap-beacon.js');
    var preconditions = beacon.checkPreconditions(req.query);

    if (preconditions.hasError) {
        res.json(preconditions.msg); //Gets error messages
        return;
    }

    var mongoQuery = beacon.buildMongoQuery(req.query);
    console.log("Building MongoDB query params: " + JSON.stringify(mongoQuery));

    req.db.samples.aggregate(mongoQuery, function(err, docs) {
        var response = beacon.checkResultAndGetResponse(req.query, docs);
        res.json(response);
    });
});

router.get('/dataset', function (req, res) {
    var preconditions = beacon.checkDatasetIdentifier(req.query);

    if (preconditions.hasError) {
        res.json(preconditions.msg);
        return;
    }

    var datasets = beacon.getDatasets(req.query);
    res.json(datasets);
});

/* Returns response for API v 0.4  */
router.get('/query/', function (req, res) {
    var preconditions = beacon.checkPreconditions(req.query);

    if (preconditions.hasError) {
        res.json(preconditions.msg); //Gets error messages
        return;
    }

    var mongoQuery = beacon.buildMongoQuery(req.query);
    console.log("Building MongoDB query params: " + JSON.stringify(mongoQuery));

    req.db.samples.aggregate(mongoQuery, function(err, docs) {
        var response = beacon.checkResultAndGetResponse(req.query, docs);
        res.json(response);
    });
});

module.exports = router;